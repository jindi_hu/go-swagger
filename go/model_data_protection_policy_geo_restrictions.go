/*
 * MCF Resources API Specification
 *
 * API Specification for MCF Resources for 3rd party provider tender
 *
 * API version: 0.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

type DataProtectionPolicyGeoRestrictions struct {

	Country string `json:"country,omitempty"`

	Policy string `json:"policy,omitempty"`
}
