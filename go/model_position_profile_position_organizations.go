/*
 * MCF Resources API Specification
 *
 * API Specification for MCF Resources for 3rd party provider tender
 *
 * API version: 0.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

type PositionProfilePositionOrganizations struct {
	// Hiring company name, unless empty, then posted company name
	Name string `json:"name,omitempty"`

	Id *PositionProfileId `json:"id,omitempty"`

	Description *PositionProfileDescription `json:"description,omitempty"`
}
