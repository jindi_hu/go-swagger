/*
 * MCF Resources API Specification
 *
 * API Specification for MCF Resources for 3rd party provider tender
 *
 * API version: 0.1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger
import (
	"time"
)

type PositionOpening struct {

	ModifiedOn time.Time `json:"modifiedOn,omitempty"`

	Data *PositionOpeningSchema `json:"data,omitempty"`
}
