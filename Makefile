DOCKER_IMAGE_NAME=jindi_hu/go-swagger
DOCKER_TAG=latest

deps:
	go mod vendor -v
	go mod tidy -v
build:
	docker build -t ${DOCKER_IMAGE_NAME}:${DOCKER_TAG} -f Dockerfile.dev .
run: build
	docker run -it \
	-p 5000:8080 \
	${DOCKER_IMAGE_NAME}:${DOCKER_TAG}
dev:
	docker-compose up --build --force-recreate  